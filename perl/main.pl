#!/usr/bin/perl
use strict;
use warnings;
use 5.010;
use JSON;
use IO::Socket::INET;
use Data::Dumper qw(Dumper);

my ($server_host, $server_port, $bot_name, $bot_key) = @ARGV;

my ($jsonUpdate,$update,$race,$key,$value);
my $doThrottlePosition = 0.3;
say "I'm $bot_name and connect to $server_host:$server_port using key $bot_key";

initialize($server_host, $server_port, $bot_name, $bot_key);

sub initialize {
  my ($server_host, $server_port, $bot_name, $bot_key) = @_;
  my $socket = new IO::Socket::INET(
    PeerHost => $server_host,
    PeerPort => $server_port,
    Proto => "tcp",
  ) or die "Failed to open TCP socket: $!\n";

  play($bot_name, $bot_key, $socket);
}

sub play {
  my ($bot_name, $bot_key, $socket) = @_;
  print $socket join_message($bot_name, $bot_key);
  react($socket);
}


sub react {
  my ($socket) = @_;
  while (<$socket>) {
    my $rawjson = $_;
    my %json = %{decode_json $_};
    my $jsonUpdate = decode_json($_);
    my $msg_type = $json{"msgType"};
    my $msg_data = $json{"data"};
    given ($msg_type) {
      when ("gameInit") {
	getTrackData(@{ $jsonUpdate->{'data'}{'race'}{'track'}{'pieces'} });
      }
      when ("carPositions") {
	logPosition(@{ $jsonUpdate->{'data'} });
        print $socket throttle_message($doThrottlePosition);
      }
      default {
        given ($msg_type) {
          when ("join") {
            say "Joined";
          }
          when ("gameStart") {
            say "Race started";
          }
          when ("crash") {
            say "Someone crashed";
          }
          when ("gameEnd") {
            say "Race ended";
          }
          when ("error") {
            say "ERROR: $msg_data";
          }
          say "Got $msg_type";
          print $socket ping_message();
        }
      }
    }
  }
}

sub join_message {
  my ($bot_name, $bot_key) = @_;
  my %msg_hash = ("name"=>$bot_name, "key"=>$bot_key);
  make_msg("join", \%msg_hash);
}

sub throttle_message {
  my ($throttle) = @_;
  make_msg("throttle",$throttle);
}

sub ping_message {
  make_msg("ping", {});
}

sub make_msg {
  my ($msg_type, $data) = @_;
  my %msg_hash = ('msgType'=>$msg_type, 'data'=>$data);
  encode_json (\%msg_hash) . "\n";
}

sub logPosition {
	my @positionUpdate = @_;
	foreach $update ( @positionUpdate ) {
		print "Update -" . " Lap " . $update->{'piecePosition'}{'lap'} . " Throttle: " . $doThrottlePosition .  " Angle: " . $update->{'angle'} . " Piece: " .  $update->{'piecePosition'}{'pieceIndex'} . " Piece Distance: " . $update->{'piecePosition'}{'inPieceDistance'} . "\n";
        }
}

sub getTrackData {
	my @trackdata = @_;
        my @corners;
        my $piececounter = 0;
        for my $pieceinfo ( @trackdata ) {
                if (exists $pieceinfo->{angle}){
                        push @corners, $piececounter;
                }
                $piececounter++;
        }
        #print Dumper \$jsonUpdate;
        print "TrackData:" . " Pieces: " . scalar @trackdata . " Corners: " . scalar @corners . " Corner pieces: ";
        foreach my $cornerpiece (@corners) {
                print " $cornerpiece ";
        }
        print "\n";
        print "--------------------------\n";
}
